package com.edb.menubackend.controllers;

import com.edb.menubackend.daos.impl.BillingAddressDaoImpl;
import com.edb.menubackend.models.BillingAddress;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/adresses/billing")
public class BillingAddressController {
    private final BillingAddressDaoImpl billingAddressDao;

    @GetMapping
    public List<BillingAddress> findAll() throws SQLException {
        return billingAddressDao.findAll();
    }

    @GetMapping("/{id}")
    public BillingAddress findById(@PathVariable int id) throws SQLException {
        return billingAddressDao.findById(id).orElse(null);
    }

    @PostMapping
    public void createBillingAddress(@RequestBody BillingAddress billingAddress) {
        billingAddressDao.save(billingAddress);
    }

    @DeleteMapping("/{id}")
    public void deleteBillingAddress(@PathVariable int id) {
        billingAddressDao.deleteById(id);
    }
}
