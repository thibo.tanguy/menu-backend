package com.edb.menubackend.controllers;

import com.edb.menubackend.daos.impl.ProductDaoImpl;
import com.edb.menubackend.models.Product;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/products")
public class ProductController {
    private final ProductDaoImpl productDao;

    @GetMapping
    public List<Product> findAll() throws SQLException {
        return productDao.findAll();
    }

    @GetMapping("/{id}")
    public Product findById(@PathVariable int id) throws SQLException {
        return productDao.findById(id).orElse(null);
    }

    @PostMapping
    public void createProduct(@RequestBody Product product) {
        productDao.save(product);
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable int id) {
        productDao.deleteById(id);
    }
}
