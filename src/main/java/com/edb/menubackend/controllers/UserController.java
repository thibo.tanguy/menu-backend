package com.edb.menubackend.controllers;


import com.edb.menubackend.daos.impl.UserDaoImpl;
import com.edb.menubackend.models.User;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api/users")
public class UserController {
    private final UserDaoImpl userDao;

    @GetMapping
    public List<User> findAll() throws SQLException {
        return userDao.findAll();
    }

    @GetMapping("/{id}")
    public User findById(@PathVariable int id) throws SQLException {
        return userDao.findById(id).orElse(null);
    }

    @PostMapping
    public void createUser(@RequestBody User user) {
        userDao.save(user);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable int id) {
        userDao.deleteById(id);
    }
}
