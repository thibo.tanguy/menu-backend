package com.edb.menubackend.controllers;

import com.edb.menubackend.daos.impl.CustomerDaoImpl;
import com.edb.menubackend.models.Customer;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/customers")
public class CustomerController {
    private final CustomerDaoImpl customerDao;

    @GetMapping
    public List<Customer> findAll() throws SQLException {
        return customerDao.findAll();
    }

    @GetMapping("/{id}")
    public Customer findById(@PathVariable int id) throws SQLException {
        return customerDao.findById(id).orElse(null);
    }

    @PostMapping
    public void createCustomer(@RequestBody Customer customer) {
        customerDao.save(customer);
    }

    @DeleteMapping("/{id}")
    public void deleteCustomer(@PathVariable int id) {
        customerDao.deleteById(id);
    }
}
