package com.edb.menubackend.controllers;

import com.edb.menubackend.daos.impl.ProductCategoryDao;
import com.edb.menubackend.models.ProductCategory;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/categories")
public class ProductCategoryController {
    private final ProductCategoryDao productCategoryDao;

    @GetMapping
    public List<ProductCategory> findAll() throws SQLException {
        return productCategoryDao.findAll();
    }

    @GetMapping("/{id}")
    public ProductCategory findById(@PathVariable int id) throws SQLException {
        return productCategoryDao.findById(id).orElse(null);
    }

    @PostMapping
    public void createProductCategory(@RequestBody ProductCategory productCategory) {
        productCategoryDao.save(productCategory);
    }

    @DeleteMapping("/{id}")
    public void deleteProductCategory(@PathVariable int id) {
        productCategoryDao.deleteById(id);
    }
}
