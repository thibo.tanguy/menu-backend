package com.edb.menubackend.controllers;

import com.edb.menubackend.daos.impl.ShippingAddressDaoImpl;
import com.edb.menubackend.models.ShippingAddress;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/addresses/shipping")
public class ShippingAddressController {
    private final ShippingAddressDaoImpl shippingAddressDao;

    @GetMapping
    public List<ShippingAddress> findAll() throws SQLException {
        return shippingAddressDao.findAll();
    }

    @GetMapping("/{id}")
    public ShippingAddress findById(@PathVariable int id) throws SQLException {
        return shippingAddressDao.findById(id).orElse(null);
    }

    @PostMapping
    public void createShippingAddress(@RequestBody ShippingAddress shippingAddress) {
        shippingAddressDao.save(shippingAddress);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable int id) {
        shippingAddressDao.deleteById(id);
    }
}
