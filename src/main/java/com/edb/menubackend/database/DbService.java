package com.edb.menubackend.database;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Getter
@Service
@AllArgsConstructor
public class DbService {
    private final DataSource dataSource;

    public <T> List<T> executeQuery(String query, ResultSetMapper<T> mapper) throws SQLException {
        List<T> results = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            try (PreparedStatement statement = connection.prepareStatement(query)) {
                try (ResultSet resultSet = statement.executeQuery()) {
                    while (resultSet.next()) {
                        results.add(mapper.map(resultSet));
                    }
                }
            }
        }

        return results;
    }
}
