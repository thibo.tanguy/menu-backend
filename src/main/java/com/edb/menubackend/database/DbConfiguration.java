package com.edb.menubackend.database;

import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public abstract class DbConfiguration {
    @Value("${app.databaseServerName}")
    private String serverName;

    @Value("${app.databasePortNumber}")
    private int portNumber;

    @Value("${app.databaseName}")
    private String databaseName;

    @Value("${app.databaseUser}")
    private String user;

    @Value("${app.databasePassword}")
    private String password;

    @Bean
    public DataSource dataSource() {
        PGSimpleDataSource dataSource = new PGSimpleDataSource();
        dataSource.setServerName(serverName);
        dataSource.setPortNumber(portNumber);
        dataSource.setDatabaseName(databaseName);
        dataSource.setUser(user);
        dataSource.setPassword(password);

        return dataSource;
    }
}

