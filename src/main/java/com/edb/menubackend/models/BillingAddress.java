package com.edb.menubackend.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillingAddress extends Address{
    public BillingAddress(int id, int userId, String address, String postalCode, String city, String country) {
        super(id, userId, address, postalCode, city, country);
    }
}
