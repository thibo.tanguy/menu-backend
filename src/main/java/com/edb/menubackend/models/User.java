package com.edb.menubackend.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class User {
    private int id;
    private String lastName;
    private String firstName;
    private String password;
    private String email;
//    private ShippingAddress shippingAddress;
//    private BillingAddress billingAddress;
}