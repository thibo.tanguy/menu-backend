package com.edb.menubackend.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Address {
    private int id;
    private int userId;
    private String address;
    private String postalCode;
    private String city;
    private String country;
//    private User user;
}
