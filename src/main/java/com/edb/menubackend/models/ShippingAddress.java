package com.edb.menubackend.models;

public class ShippingAddress extends Address {
    public ShippingAddress(int id, int userId, String address, String postalCode, String city, String country) {
        super(id, userId, address, postalCode, city, country);
    }
}
