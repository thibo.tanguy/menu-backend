package com.edb.menubackend.models;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Client {
    private int id;
    private Customer customer;
}
