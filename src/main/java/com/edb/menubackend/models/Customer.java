package com.edb.menubackend.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Customer {
    private int id;
    private String lastname;
    private String firstname;
    private String company;
    private String mail;
    private String phone;
    private String mobile;
    private String notes;
    private boolean active;
}
