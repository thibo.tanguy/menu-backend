package com.edb.menubackend.daos.interfaces;

import com.edb.menubackend.models.ProductCategory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface IProductCategoryDao {
    List<ProductCategory> findAll() throws SQLException;
    Optional<ProductCategory> findById(int id) throws SQLException;
    void save(ProductCategory customer);
    void deleteById(int id);
    ProductCategory dataToObject(ResultSet resultSet) throws SQLException;
}
