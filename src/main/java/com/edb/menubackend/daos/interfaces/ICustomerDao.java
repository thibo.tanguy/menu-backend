package com.edb.menubackend.daos.interfaces;


import com.edb.menubackend.models.Customer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface ICustomerDao {
    List<Customer> findAll() throws SQLException;
    Optional<Customer> findById(int id) throws SQLException;
    void save(Customer customer);
    void deleteById(int id);
    Customer dataToObject(ResultSet resultSet) throws SQLException;
}
