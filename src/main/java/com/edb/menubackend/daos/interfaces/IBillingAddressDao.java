package com.edb.menubackend.daos.interfaces;

import com.edb.menubackend.models.BillingAddress;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface IBillingAddressDao {
    List<BillingAddress> findAll() throws SQLException;
    Optional<BillingAddress> findById(int id) throws SQLException;
    void save(BillingAddress billingAddress);
    void deleteById(int id);
    BillingAddress dataToObject(ResultSet resultSet) throws SQLException;
}
