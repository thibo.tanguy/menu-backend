package com.edb.menubackend.daos.interfaces;


import com.edb.menubackend.models.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;


public interface IUserDao {
    List<User> findAll() throws SQLException;
    Optional<User> findById(int id) throws SQLException;
    void save(User user);
    void deleteById(int id);
    User dataToObject(ResultSet resultSet) throws SQLException;
}
