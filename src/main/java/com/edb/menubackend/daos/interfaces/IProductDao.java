package com.edb.menubackend.daos.interfaces;

import com.edb.menubackend.models.Product;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface IProductDao {
    List<Product> findAll();
    Optional<Product> findById(int id);
    void save(Product product);
    void deleteById(int id);
    Product dataToObject(ResultSet resultSet) throws SQLException;
}
