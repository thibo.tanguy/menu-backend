package com.edb.menubackend.daos.interfaces;

import com.edb.menubackend.models.ShippingAddress;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface IShippingAddress {
    List<ShippingAddress> findAll() throws SQLException;
    Optional<ShippingAddress> findById(int id) throws SQLException;
    void save(ShippingAddress shippingAddress);
    void deleteById(int id);
    ShippingAddress dataToObject(ResultSet resultSet) throws SQLException;
}
