package com.edb.menubackend.daos.impl;

import com.edb.menubackend.database.DbService;
import com.edb.menubackend.models.Product;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
@AllArgsConstructor
public class ProductDaoImpl {
    private final DbService dbService;

    public List<Product> findAll() throws SQLException {
        String query = "SELECT * FROM products";

        return dbService.executeQuery(query, this::dataToObject);
    }

    public Optional<Product> findById(int id) throws SQLException {
        String query = "SELECT * FROM products WHERE id = ?";
        List<Product> results = dbService.executeQuery(query, resultSet -> resultSet.next() ? dataToObject(resultSet) : null);
        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }

    public void save(Product product) {
        String request = "INSERT INTO products (name, description, price, quantity) VALUES (?, ?, ?, ?)";

        try (Connection connection = dbService.getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(request)) {
            statement.setString(1, product.getName());
            statement.setString(2, product.getDescription());
            statement.setDouble(3, product.getPrice());
            statement.setInt(4, product.getQuantity());

            statement.executeUpdate();
        }

        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void deleteById(int id) {
        String request = "DELETE FROM products WHERE id = ?;";

        try (Connection connection = dbService.getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(request)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        }

        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public Product dataToObject(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        String description = resultSet.getString("description");
        double price = resultSet.getDouble("price");
        int quantity = resultSet.getInt("quantity");

        return new Product(id, name, description, price, quantity);
    }
}
