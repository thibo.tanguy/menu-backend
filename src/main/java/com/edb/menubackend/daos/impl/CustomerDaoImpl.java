package com.edb.menubackend.daos.impl;

import com.edb.menubackend.daos.interfaces.ICustomerDao;
import com.edb.menubackend.models.Customer;
import com.edb.menubackend.database.DbService;
import com.edb.menubackend.models.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
@AllArgsConstructor
public class CustomerDaoImpl implements ICustomerDao {
    private final DbService dbService;

    @Override
    public List<Customer> findAll() throws SQLException {
        String query = "SELECT * FROM customers;";

        return dbService.executeQuery(query, this::dataToObject);
    }

    @Override
    public Optional<Customer> findById(int id) throws SQLException {
        String query = "SELECT * FROM customers WHERE id = ?;";

        List<Customer> results = dbService.executeQuery(query, resultSet -> resultSet.next() ? dataToObject(resultSet) : null);

        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }

    @Override
    public void save(Customer customer) {
        String request = "INSERT INTO customers (lastname, firstname, company, mail, phone, mobile, notes, active) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        try (Connection connection = dbService.getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(request)) {
            statement.setString(1, customer.getLastname());
            statement.setString(2, customer.getFirstname());
            statement.setString(3, customer.getCompany());
            statement.setString(4, customer.getMail());
            statement.setString(5, customer.getPhone());
            statement.setString(6, customer.getMobile());
            statement.setString(7, customer.getNotes());
            statement.setBoolean(8, customer.isActive());

            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void deleteById(int id) {
        String request = "DELETE FROM customers WHERE id = ?;";

        try (Connection connection = dbService.getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(request)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public Customer dataToObject(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String lastname = resultSet.getString("lastname");
        String firstname = resultSet.getString("firstname");
        String company = resultSet.getString("company");
        String mail = resultSet.getString("mail");
        String phone = resultSet.getString("phone");
        String mobile = resultSet.getString("mobile");
        String notes = resultSet.getString("notes");
        boolean active = resultSet.getBoolean("active");

        return new Customer(id, lastname, firstname, company, mail, phone, mobile, notes, active);
    }
}
