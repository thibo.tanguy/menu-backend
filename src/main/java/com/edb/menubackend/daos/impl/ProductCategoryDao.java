package com.edb.menubackend.daos.impl;

import com.edb.menubackend.daos.interfaces.IProductCategoryDao;
import com.edb.menubackend.models.ProductCategory;
import com.edb.menubackend.database.DbService;
import com.edb.menubackend.models.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
@AllArgsConstructor
public class ProductCategoryDao implements IProductCategoryDao {
    private final DbService dbService;

    @Override
    public List<ProductCategory> findAll() throws SQLException {
        String query = "SELECT * FROM product_categories;";

        return dbService.executeQuery(query, this::dataToObject);
    }

    @Override
    public Optional<ProductCategory> findById(int id) throws SQLException {
        String query = "SELECT * FROM product_categories WHERE id = ?;";
        List<ProductCategory> results = dbService.executeQuery(query, resultSet -> resultSet.next() ? dataToObject(resultSet) : null);

        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }

    @Override
    public void save(ProductCategory productCategory) {
        String request = "INSERT INTO product_categories (name) VALUES (?)";

        try (Connection connection = dbService.getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(request)) {
            statement.setString(1, productCategory.getName());

            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void deleteById(int id) {
        String request = "DELETE FROM product_categories WHERE id = ?;";

        try (Connection connection = dbService.getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(request)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public ProductCategory dataToObject(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");

        return new ProductCategory(id, name);
    }
}
