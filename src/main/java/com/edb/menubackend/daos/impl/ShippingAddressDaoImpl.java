package com.edb.menubackend.daos.impl;

import com.edb.menubackend.daos.interfaces.IShippingAddress;
import com.edb.menubackend.database.DbService;
import com.edb.menubackend.models.ShippingAddress;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
@AllArgsConstructor
public class ShippingAddressDaoImpl implements IShippingAddress {
    private final DbService dbService;

    @Override
    public List<ShippingAddress> findAll() throws SQLException {
        String query = "SELECT * FROM shipping_addresses;";

        return dbService.executeQuery(query, this::dataToObject);
    }

    @Override
    public Optional<ShippingAddress> findById(int id) throws SQLException {
        String query = "SELECT * FROM shipping_addresses WHERE id = ?;";
        List<ShippingAddress> results = dbService.executeQuery(query, resultSet -> resultSet.next() ? dataToObject(resultSet) : null);

        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }

    @Override
    public void save(ShippingAddress shippingAddress) {
        String request = "INSERT INTO shipping_addresses (name, description, price, quantity) VALUES (?, ?, ?, ?)";

        try (Connection connection = dbService.getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(request)) {
//            statement.setInt(1, shippingAddress.getUserId());
            statement.setString(2, shippingAddress.getAddress());
            statement.setString(3, shippingAddress.getPostalCode());
            statement.setString(4, shippingAddress.getCity());
            statement.setString(5, shippingAddress.getCountry());

            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void deleteById(int id) {
        String request = "DELETE FROM shipping_addresses WHERE id = ?;";

        try (Connection connection = dbService.getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(request)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public ShippingAddress dataToObject(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        int userId = resultSet.getInt("user_id");
        String address = resultSet.getString("name");
        String postalCode = resultSet.getString("description");
        String city = resultSet.getString("price");
        String country = resultSet.getString("quantity");
//        Optional<User> user = userDao.findById(userId);

        return new ShippingAddress(id, userId, address, postalCode, city, country);
    }
}
