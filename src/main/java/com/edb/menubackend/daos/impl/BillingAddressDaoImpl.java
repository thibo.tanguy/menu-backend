package com.edb.menubackend.daos.impl;

import com.edb.menubackend.daos.interfaces.IBillingAddressDao;
import com.edb.menubackend.models.BillingAddress;
import com.edb.menubackend.database.DbService;
import com.edb.menubackend.models.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
@AllArgsConstructor
public class BillingAddressDaoImpl implements IBillingAddressDao {
    private final DbService dbService;

    @Override
    public List<BillingAddress> findAll() throws SQLException {
        String query = "SELECT * FROM billing_addresses";

        return dbService.executeQuery(query, this::dataToObject);
    }

    @Override
    public Optional<BillingAddress> findById(int id) throws SQLException {
        String query = "SELECT * FROM billing_addresses WHERE id = ?";
        List<BillingAddress> results = dbService.executeQuery(query, resultSet -> resultSet.next() ? dataToObject(resultSet) : null);

        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }

    @Override
    public void save(BillingAddress billingAddress) {
        String query = "INSERT INTO billing_addresses (user_id, address, postalCode, city, country) VALUES (?, ?, ?, ?, ?)";

        try (Connection connection = dbService.getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, billingAddress.getUserId());
            statement.setString(2, billingAddress.getAddress());
            statement.setString(3, billingAddress.getPostalCode());
            statement.setString(4, billingAddress.getCity());
            statement.setString(5, billingAddress.getCountry());
            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error while saving billing address: " + e.getMessage());
        }
    }

    @Override
    public void deleteById(int id) {
        String query = "DELETE FROM billing_addresses WHERE id = ?";

        try (Connection connection = dbService.getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Error while deleting billing address: " + e.getMessage());
        }
    }

    @Override
    public BillingAddress dataToObject(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        int userId = resultSet.getInt("user_id");
        String address = resultSet.getString("address");
        String postalCode = resultSet.getString("postalCode");
        String city = resultSet.getString("city");
        String country = resultSet.getString("country");

        return new BillingAddress(id, userId, address, postalCode, city, country);
    }
}
