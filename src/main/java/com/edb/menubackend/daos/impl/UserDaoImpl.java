package com.edb.menubackend.daos.impl;

import com.edb.menubackend.daos.interfaces.IUserDao;
import com.edb.menubackend.database.DbService;
import com.edb.menubackend.models.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@Repository
@AllArgsConstructor
public class UserDaoImpl implements IUserDao {
    private final DbService dbService;

    @Override
    public List<User> findAll() throws SQLException {
        String query = "SELECT * FROM users;";

        return dbService.executeQuery(query, this::dataToObject);
    }

    @Override
    public Optional<User> findById(int id) throws SQLException {
        String query = "SELECT * FROM users WHERE id = ?;";
        List<User> results = dbService.executeQuery(query, resultSet -> resultSet.next() ? dataToObject(resultSet) : null);

        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));

    }

    @Override
    public void save(User user) {
        String request = "INSERT INTO users (last_name, first_name, email, password) VALUES (?, ?, ?, ?)";

        try (Connection connection = dbService.getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(request)) {
            statement.setString(1, user.getLastName());
            statement.setString(2, user.getFirstName());
            statement.setString(3, user.getEmail());
            statement.setString(4, user.getPassword());

            statement.executeUpdate();
        }

        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void deleteById(int id) {
        String request = "DELETE FROM users WHERE id = ?;";

        try (Connection connection = dbService.getDataSource().getConnection();
             PreparedStatement statement = connection.prepareStatement(request)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        }

        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public User dataToObject(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String lastname = resultSet.getString("last_name");
        String firstname = resultSet.getString("first_name");
        String password = resultSet.getString("password");
        String email = resultSet.getString("email");

        return new User(id, firstname, lastname, email, password);
    }
}
