## Installation

Tapez la commande suivante :

```
docker-compose up -d
```

Le swagger est désormais accessible à l'url : http://localhost:8080/swagger-ui/index.html