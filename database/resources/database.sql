CREATE DATABASE menus;
\c menus;

CREATE TABLE product_categories
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR(50)
);

CREATE TABLE products
(
    id                  SERIAL PRIMARY KEY,
    product_category_id INT,
    name                VARCHAR(50),
    description         TEXT,
    price               DECIMAL(10, 2),
    quantity            INT,
    FOREIGN KEY (product_category_id) REFERENCES product_categories (id)
);

CREATE TABLE users
(
    id         SERIAL PRIMARY KEY,
    last_name  VARCHAR(50),
    first_name VARCHAR(50),
    email      VARCHAR(50),
    password   VARCHAR(50)
);

CREATE TABLE customers
(
    id      SERIAL PRIMARY KEY,
    user_id INT,
    FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE shipping_addresses
(
    id          SERIAL PRIMARY KEY,
    user_id     INT,
    address     VARCHAR(100),
    postal_code VARCHAR(10),
    city        VARCHAR(50),
    country     VARCHAR(50),
    FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE billing_addresses
(
    id          SERIAL PRIMARY KEY,
    user_id     INT,
    address     VARCHAR(100),
    postal_code VARCHAR(10),
    city        VARCHAR(50),
    country     VARCHAR(50),
    FOREIGN KEY (user_id) REFERENCES users (id)
);

INSERT INTO product_categories (id, name)
VALUES (1, 'Electronics'),
       (2, 'Clothing'),
       (3, 'Books'),
       (4, 'Home Appliances'),
       (5, 'Toys');

INSERT INTO products (id, product_category_id, name, description, price, quantity)
VALUES (1, 1, 'Laptop', 'High-performance laptop with SSD storage', 1200.00, 50),
       (2, 1, 'Smartphone', 'Latest model with advanced camera features', 800.00, 100),
       (3, 2, 'T-Shirt', 'Casual cotton t-shirt in various colors', 20.00, 200),
       (4, 3, 'Java Programming Book', 'Comprehensive guide to Java programming', 35.00, 30),
       (5, 4, 'Refrigerator', 'Energy-efficient refrigerator with frost-free feature', 900.00, 20);

INSERT INTO users (id, last_name, first_name, email, password)
VALUES (1, 'Doe', 'John', 'john.doe@example.com', 'password123'),
       (2, 'Smith', 'Jane', 'jane.smith@example.com', 'abc123'),
       (3, 'Johnson', 'Michael', 'michael.johnson@example.com', 'securepwd'),
       (4, 'Brown', 'Sarah', 'sarah.brown@example.com', 'userpass'),
       (5, 'Davis', 'Emily', 'emily.davis@example.com', 'p@ssw0rd');

INSERT INTO customers (id, user_id)
VALUES (1, 1),
       (2, 2),
       (3, 3),
       (4, 4),
       (5, 5);

INSERT INTO shipping_addresses (id, user_id, address, postal_code, city, country)
VALUES (1, 1, '123 Main St', '12345', 'Anytown', 'USA'),
       (2, 2, '456 Elm St', '67890', 'Sometown', 'USA'),
       (3, 3, '789 Oak St', '54321', 'Othertown', 'USA'),
       (4, 4, '321 Pine St', '98765', 'Anycity', 'USA'),
       (5, 5, '654 Maple St', '13579', 'Somewhere', 'USA');

INSERT INTO billing_addresses (id, user_id, address, postal_code, city, country)
VALUES (1, 1, '123 Main St', '12345', 'Anytown', 'USA'),
       (2, 2, '456 Elm St', '67890', 'Sometown', 'USA'),
       (3, 3, '789 Oak St', '54321', 'Othertown', 'USA'),
       (4, 4, '321 Pine St', '98765', 'Anycity', 'USA'),
       (5, 5, '654 Maple St', '13579', 'Somewhere', 'USA');
