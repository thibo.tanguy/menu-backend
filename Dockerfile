# Build
FROM openjdk:17-jdk-alpine AS build-env
WORKDIR /sdk
RUN apk add --no-cache maven
COPY pom.xml .
RUN mvn dependency:go-offline -B
WORKDIR /app
COPY . .
RUN mvn package

# Run
FROM openjdk:17-jdk-alpine
WORKDIR /app
COPY --from=build-env /app/target/menu-backend-0.0.1-SNAPSHOT.jar .
EXPOSE 8080
CMD ["java", "-jar", "menu-backend-0.0.1-SNAPSHOT.jar"]
